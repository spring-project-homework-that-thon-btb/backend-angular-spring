package angulars.org.springangular.models;


import javax.persistence.*;

@Entity
public class Memo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String subject;
    private String content;
    private Integer memoCateId;
    @Column(columnDefinition="Integer default 0")
    private Integer imp;
    private String bgColor;
    private String regdt;
    private String moddt;
    private String userId;

    public Memo() {
    }

    public Memo(String subject, String content, Integer memoCateId, Integer imp, String bgColor, String regdt, String moddt, String userId) {
        this.subject = subject;
        this.content = content;
        this.memoCateId = memoCateId;
        this.imp = imp;
        this.bgColor = bgColor;
        this.regdt = regdt;
        this.moddt = moddt;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getMemoCateId() {
        return memoCateId;
    }

    public void setMemoCateId(Integer memoCateId) {
        this.memoCateId = memoCateId;
    }

    public Integer getImp() {
        return imp;
    }

    public void setImp(Integer imp) {
        this.imp = imp;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getRegdt() {
        return regdt;
    }

}
