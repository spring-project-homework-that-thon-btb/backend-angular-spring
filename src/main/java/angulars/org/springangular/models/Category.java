package angulars.org.springangular.models;

import javax.persistence.*;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String bgColor;
    private String regdt;
    private String moddt;
    private String userId;

    public Category() {
    }

    public Category(String name, String bgColor, String regdt, String moddt, String userId) {
        this.name = name;
        this.bgColor = bgColor;
        this.regdt = regdt;
        this.moddt = moddt;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getRegdt() {
        return regdt;
    }

    public void setRegdt(String regdt) {
        this.regdt = regdt;
    }

    public String getModdt() {
        return moddt;
    }

    public void setModdt(String moddt) {
        this.moddt = moddt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
