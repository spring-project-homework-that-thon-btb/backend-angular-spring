package angulars.org.springangular.controllers;

import angulars.org.springangular.models.Category;
import angulars.org.springangular.repositories.CategoryRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryRepositories categoryRepositories;

    @GetMapping
    private void saveAllCategories() {
        List<Category> categories = new ArrayList<>();
        String dtPattern = "yyyyMMddhhmmss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dtPattern);
        String date = simpleDateFormat.format(new Date());
        categories.add(new Category("IT Infrastructure", "#0000", date, date, "coca"));
        categories.add(new Category("Management Team", "#0000", date, date, "coca"));
        categories.add(new Category("Motivation for application", "#0000", date, date, "coca"));
        categoryRepositories.saveAll(categories);
    }


    @GetMapping("/all")
    private List<Category> getAllCategories() {
        System.out.println("show all categories");
        return (List<Category>) categoryRepositories.findAll();
    }


}
