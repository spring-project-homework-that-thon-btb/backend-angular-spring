package angulars.org.springangular.controllers;


import angulars.org.springangular.models.Memo;
import angulars.org.springangular.repositories.MemoRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/memo")
public class MemoController {

    @Autowired
    private MemoRepositories memoRepositories;

    @GetMapping
    private void saveAllMemo() {
        List<Memo> memos = new ArrayList<>();
        String dtPattern = "yyyyMMddhhmmss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dtPattern);
        String date = simpleDateFormat.format(new Date());
        System.out.println(date);
        memos.add(new Memo("Chairman", "Chairman Speech", 1, 0, "#1111", date, date, "coca"));
        memos.add(new Memo("Chairman", "Chairman Speech", 1, 0, "#1111", date, date, "coca"));
        memos.add(new Memo("Chairman", "Chairman Speech", 1, 0, "#1111", date, date, "coca"));

        memos.add(new Memo("Chairman", "Chairman Speech", 2, 0, "#1111", date, date, "coca"));
        memos.add(new Memo("Chairman", "Chairman Speech", 2, 0, "#1111", date, date, "coca"));
        memos.add(new Memo("Chairman", "Chairman Speech", 2, 0, "#1111", date, date, "coca"));

        memos.add(new Memo("Chairman", "Chairman Speech", 3, 0, "#1111", date, date, "coca"));
        memos.add(new Memo("Chairman", "Chairman Speech", 3, 0, "#1111", date, date, "coca"));
        memos.add(new Memo("Chairman", "Chairman Speech", 3, 0, "#1111", date, date, "coca"));
        memoRepositories.saveAll(memos);
    }


    @GetMapping("/all")
    public List<Memo> getAllMemos() {
        System.out.println("Java Developers, Please enjoy together...");
        System.out.println("Learning from school...");
        System.out.println("Java Learning more than...");
        return (List<Memo>) memoRepositories.findAll();
    }


}
