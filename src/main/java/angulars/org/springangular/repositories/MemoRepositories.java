package angulars.org.springangular.repositories;


import angulars.org.springangular.models.Memo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemoRepositories extends CrudRepository<Memo,Long> {

}
