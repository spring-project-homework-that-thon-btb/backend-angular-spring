package angulars.org.springangular.repositories;

import angulars.org.springangular.models.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepositories extends CrudRepository<Category,Long> {
}
